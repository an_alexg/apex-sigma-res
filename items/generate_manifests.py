# Apex Sigma: The Database Giant Discord Bot.
# Copyright (C) 2018  Lucia's Cipher
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import glob

import yaml


class ManifestGenerator(object):
    def __init__(self):
        self.file_paths = []
        self.items = []
        self.recipes = []

    def walk(self):
        print('Getting all item YML files...')
        for file in glob.glob('individuals/**/*.yml', recursive=True):
            self.file_paths.append(file)

    @staticmethod
    def unify_item_data(recipe: bool, path: str, data: dict):
        out = {
            'name': data.get('name'),
            'description': data.get('description'),
            'type': data.get('type'),
            'file_id': path.split('/')[-1].split('\\')[-1].split('.')[0]
        }
        if not recipe:
            out.update({'rarity': data.get('rarity')})
        return out

    @staticmethod
    def unify_recipe_data(path: str, data: dict):
        return {
            'name': data.get('name'),
            'description': data.get('description'),
            'type': data.get('type'),
            'ingredients': data.get('ingredients'),
            'file_id': path.split('/')[-1].split('\\')[-1].split('.')[0]
        }

    def process_items(self):
        print('Processing item data...')
        for item_file_path in self.file_paths:
            with open(item_file_path, 'r', encoding='utf-8') as item_file:
                item_data = yaml.safe_load(item_file)
                is_recipe = 'recipes' in item_file_path
                self.items.append(self.unify_item_data(is_recipe, item_file_path, item_data))
                if is_recipe:
                    self.recipes.append(self.unify_recipe_data(item_file_path, item_data))
        print(f'Processed {len(self.file_paths)} item files.')

    def dump_items(self):
        print('Dumping item manifest...')
        with open('item_core_manifest.yml', 'w') as item_core_file:
            yaml.safe_dump(self.items, item_core_file, default_flow_style=False)

    def dump_recipes(self):
        print('Dumping recipe manifest...')
        with open('recipe_core_manifest.yml', 'w') as recipe_core_file:
            yaml.safe_dump(self.recipes, recipe_core_file, default_flow_style=False)

    def run(self):
        self.walk()
        self.process_items()
        self.dump_items()
        self.dump_recipes()
        print('All done!')


mfg = ManifestGenerator()
mfg.run()
